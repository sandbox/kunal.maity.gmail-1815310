$Id: README.txt,v 1.0 2012/10/17 13:29:13  Exp $

CONTENTS OF THIS FILE
---------------------

 * INTRODUCTION
 * FEATURES
 * INSTALLATION
 * USAGE


INTRODUCTION
------------

Current Maintainers: Chittaranjan Pattnaik
Co-maintainer: Somesh Sen
Original Author: Kunal Maity

Prevent Hacking module helps your site URLs and Form elements from entering hacking or vulnerable character. This module helps in automating this process instead of manually applying this rectriction for URLs or Form elements.

FEATURES
------------

Once you enable this module the following features will be available:
- It will check all URLs from any vulnerable character or SQL injection.
- It will check all form elements from any vulnerable character or SQL injection.
- It's fully configurable which allows the user to set the different vulneable character/condition that they come across to prevent their site from any harmful attack.
- It allows site administrator to exclude or include forms from this check.
- Excluding and Including forms made easy by providing a link under each form to perform the required action when the current user logged in is site administrator only.
- If any harmful character found then error messages and redirections are configurable from admin end.
- It validates all values of GET/POST/REQUEST parameters to prevent from any vulnerable code entry.

INSTALLATION
------------

Once Prevent Hacking is installed and enabled, you can adjust the settings for your
site's hacking prevention at admin/pre-hack.

It is highly recommended that you read the below instructions carefully before using this module.

USAGE
------------

admin/pre-hack/configuration:
The usage/configuration of this module is intended for site administrator only. Once site administrator set the appropriate settings then the module will restrict all vulnerable URLs and specified forms.

Under the configuration settings page following are the settings available:
Display Form ID Include/Exclude Link (Checkbox): If this is checked then the link to include/exclude a form from this check will show on all the form on the site when the logged in user is site administrator.

Excluded Form Ids (TextArea - Disabled): This shows all the form ids comma (,) separated that are excluded from this check. Which mean when an excluded form will be submitted then this check will be by-passed.

URL protection (TextArea): This field allows the site administrator to enter all the different harmful, vulnerable and hacking character/words separated by hash (#), that they want to restrict if found in URLs.

Redirect Page (TextField): This field stores the page to redirect when there any harmful/restriced character/word found (checked against conditions available in URL Protection field) in the URL.

FORM Field Protection (TextArea): This field allows the site administrator to enter all the different harmful, vulnerable and hacking character/words separated by comma (,) that they want to restrict if found in Form fields/elements.

URL error message (TextArea): This is a configurable error message to be shown after redirecting if any harmful characters found in URLs.

Form elements error message (Textarea): This is a configurable error message to be shown as validation error if any harmful characters found in form elements.

Excluding Form ID Link Text (TextArea): This text will be shown under all forms as a link to exclude a form from this check.

Including Form ID Link Text (TextArea): This text will be shown under all forms as a link to include an excluded form in this check.


Important Instruction
----------------------
-  This module's admin configuration will be available for site's super admin only, i.e. only for user ID 1.
-  In time of building form it is mandatory that you have to use Drupal's form API or WebForm. Simple HTML form will not be validated by this module.
 

